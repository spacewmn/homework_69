import React from 'react';
import SeriesFinder from "./containers/SeriesFinder/SeriesFinder";
import Layout from "./components/Layout/Layout";
import {Route, Switch} from 'react-router-dom';
import Info from "./components/Info/Info";

const App = () => {
  return (
      <Layout>
        <Switch>
          <Route path="/pages/:id" exact component={Info}/>
        </Switch>
      </Layout>
  );
};


export default App;
