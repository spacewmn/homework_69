import React, {Fragment} from 'react';
import SeriesFinder from "../../containers/SeriesFinder/SeriesFinder";

const Layout = (props) => {
    return (
        <Fragment>
            <header>
                <SeriesFinder/>
            </header>
            {props.children}
        </Fragment>
    );
};

export default Layout;