import React, {useEffect, useReducer} from 'react';
import {findingSeries} from "../../containers/SeriesFinder/SeriesFinder";
import instanceID from "../../axios-series-id";
import {FINDING_SERIES, INFO_SERIES} from "../../store/actionTypes";
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';

const initialState = {
    infoSeries: null,
    error: null
};

const reducer = (state, action) => {
    switch (action.type) {
        case INFO_SERIES:
            return {...state, infoSeries: action.value}
        default:
            return state;
    }
}

export const infoSeries = (value) => {
    return {type: INFO_SERIES, value}
};


const Info = (props) => {
    const [state, dispatch] = useReducer(reducer, initialState);


    useEffect(() => {
        const fetchData = async () => {
            const response = await instanceID.get(props.match.params.id);
            dispatch(infoSeries(response.data));

            // console.log(response.data);
        };
        fetchData().catch(console.error);

    }, [props.match.params.id]);

    let img = '';

    return state.infoSeries && (
        <div className="container">
            <h1>{state.infoSeries.name}</h1>
            {state.infoSeries.image ? <img src={state.infoSeries.image.medium} alt="#"/> : null}
            <div>{state.infoSeries.summary}</div>
        </div>
    );
};

export default Info;