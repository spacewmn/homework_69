import instance from "../axios-series";
import {
    CURRENT_SERIES,
    FETCH_ERROR,
    FETCH_REQUEST,
    FETCH_SUCCESS,
} from "./actionTypes";

const fetchRequest = () => {
    return {type: FETCH_REQUEST};
};
const fetchSuccess = value => {
    return {type: FETCH_SUCCESS, value};
};
const fetchError = error => {
    return {type: FETCH_ERROR, error};
};
