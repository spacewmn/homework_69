import axios from 'axios';

const instanceID = axios.create({
    baseURL: 'http://api.tvmaze.com/shows/'
});

export default instanceID;